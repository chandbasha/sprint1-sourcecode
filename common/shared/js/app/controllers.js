angular.module('app.controllers', [])

.controller('side-menu21Ctrl',function ($scope, $state ) {	
	$scope.logout = function(){
		$state.go("login");
		
	};	
})

.controller('hONDACtrl', function ($scope, $state,hondaService, $rootScope) {	
	
	var collectionName = 'jsonStoreDivisionID';
	WL.JSONStore.get(collectionName).findAll().then(function (arrayResults) {
		//WL.Logger.debug(" JSON Store--->"+JSON.stringify(arrayResults));
		
		 if(arrayResults[0].json.divisionID == 'A'){
			 if($rootScope.loginSessionId == '183b657e-2ffd-409c-8a66-0d9efcd4bdda'){
				  $scope.response = hondaService.homeData();
			  }else if($rootScope.loginSessionId == '183b657e-2ffd-409c-8a66-0d9efcd4bddb'){
				  $scope.response = hondaService.hondaSalesHomeData();
			  }else if($rootScope.loginSessionId == '183b657e-2ffd-409c-8a66-0d9efcd4bddc'){
					  $scope.response = hondaService.hondaServiceHomeData();
			  }
			
		  }
	}).fail(function (error) {
	});
})

/*.controller('login-home', function ($scope, $state,hondaService, $rootScope) {
	var collectionName = 'jsonStoreDivisionID';
	WL.JSONStore.get(collectionName).findAll().then(function (arrayResults) {
		//WL.Logger.debug(" JSON Store--->"+JSON.stringify(arrayResults));
		 if(arrayResults[0].json.divisionID == 'A'){
			 if($rootScope.loginSessionId == '183b657e-2ffd-409c-8a66-0d9efcd4bdda'){
				  $scope.response = hondaService.homeData();
			  }else if($rootScope.loginSessionId == '183b657e-2ffd-409c-8a66-0d9efcd4bddb'){
				  $scope.response = hondaService.hondaSalesHomeData();
			  }else if($rootScope.loginSessionId == '183b657e-2ffd-409c-8a66-0d9efcd4bddc'){
					  $scope.response = hondaService.hondaServiceHomeData();
			  }
			
		  }
	}).fail(function (error) {
	});
	
	
})*/
.controller('dealerSubmenuCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {


}])

.controller('dealerDashboardCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {


}])

.controller('vINCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {


}])

/*.controller('loginCtrl', function ($scope, $state, hondaService, $rootScope) {
	$scope.divisionLogin = function(event) {
		
		var dealerValue = $scope.dealer;
		var userValue = $scope.user;
		var passwordValue = $scope.password;
		
		if(typeof dealerValue === "undefined" || typeof userValue === "undefined" || typeof passwordValue === "undefined" )
		{
			$scope.error = "All authentication parameters must be provided!";
			$state.go('login');
		}else{
			  var sessionId;
			  var logonID;
			  $scope.response = hondaService.authenticate(dealerValue, userValue, passwordValue);
			 
			  
			  var res = $scope.response;
			  if(res == "NOT_MATCHED"){
				  $scope.error = "Incorrect credentials entered. Please try again.";
				  $state.go('login');
			  }else{
				  res = JSON.parse($scope.response);				  
				  if(res.serviceresult.success == true){
					  $rootScope.loginSessionId =  res.serviceresult.data.sessioninfo.SessionID;
					 // $rootScope.scopeDivisionID =  res.serviceresult.data.sessioninfo.ProdDivCode;
					  var collectionName = 'jsonStoreDivisionID';
					//  var options = {localKeyGen : true};
					  
					  
              		var data = [{divisionID: res.serviceresult.data.sessioninfo.ProdDivCode}];

              		WL.JSONStore.get(collectionName).add(data).then(function (numberOfDocumentsAdded) {
						 // $state.go('login-home');	
              			 $state.go('tabsController.hONDA_tab3');	
					  }).fail(function (error) {
					  });
			
				  }else{
					  $scope.error = "No data availble.";
					  $state.go('login');
				  }
			  }
		}
	  };
})*/

.controller('login-genericCtrl', function ($scope, $state, hondaService, $rootScope) {
	
	
	
	$scope.genericSubmit = function(event) {		
		var dealerValue = $scope.dealer;
		var userValue = $scope.user;
		var passwordValue = $scope.password;
		
		if(typeof dealerValue === "undefined" || typeof userValue === "undefined" || typeof passwordValue === "undefined" )
		{
			$scope.error = "All authentication parameters must be provided!";
			$state.go('login-generic');
		}else{
			  var sessionId;
			  var logonID;
			  $scope.response = hondaService.authenticate(dealerValue, userValue, passwordValue);			 
			  
			  var res = $scope.response;
			  if(res == "NOT_MATCHED"){
				  $scope.error = "Incorrect credentials entered. Please try again.";
				  $state.go('login-generic');
			  }else{
				  res = JSON.parse($scope.response);				  
				  if(res.serviceresult.success == true){
					  $rootScope.loginSessionId =  res.serviceresult.data.sessioninfo.SessionID;
					//  $rootScope.scopeDivisionID =  res.serviceresult.data.sessioninfo.ProdDivCode;
					  var collectionName = 'jsonStoreDivisionID';
              		var data = [{divisionID: res.serviceresult.data.sessioninfo.ProdDivCode}];
              		WL.JSONStore.get(collectionName).add(data).then(function (numberOfDocumentsAdded) {
              			 alert("second");
						  $state.go('tabsController.hONDA_tab3');	
						
					  }).fail(function (error) {
					  });
			
				  }else{
					  $scope.error = "No data availble.";
					  $state.go('login-generic');
				  }
			  }
		}
	  };
})



.controller('genericChangePasswordCtrl', ['$scope', '$stateParams', '$rootScope',
       function ($scope, $stateParams, $rootScope) {
	$scope.showGenericLogo = true;
    $scope.showHondoLogo= false;
	var collectionName = 'jsonStoreDivisionID';
	//alert("genericChangePasswordCtrl");
	/*WL.JSONStore.get(collectionName).findAll().then(function (arrayResults) {
		//WL.Logger.debug(" JSON Store--->"+JSON.stringify(arrayResults));
		//alert("genericChangePasswordCtrl"+arrayResults[0].json.divisionID)
		 if(arrayResults[0].json.divisionID == 'A'){
			 
			 $scope.showGenericLogo = false;
             $scope.showHondoLogo= true;
         }else {
         	$scope.showGenericLogo = true;
             $scope.showHondoLogo= false;
         }     
			
	}).fail(function (error) {
	});*/
			/*if($rootScope.scopeDivisionID != 'A'){	
       	 		$scope.showGenericLogo = true;
                $scope.showHondoLogo= false;
            }else if($rootScope.scopeDivisionID == 'A'){
            	$scope.showGenericLogo = false;
                $scope.showHondoLogo= true;
            }   */  
			
			$scope.resetForm = function ()
		    {
				$scope.changepwd.dealer =  '';
				$scope.changepwd.newpass =  '';
				$scope.changepwd.confirmnewpass =  '';
		    };
       	 

        $scope.char8=0;
        $scope.capital=0;
        $scope.isNum=0;
        $scope.isMatch =0;
        $scope.changepwd={};
        $scope.validate =  function (){
          var val = $scope.changepwd.newpass;
         if (val.length>=8){
          $scope.char8 = 1;
         } else {
          $scope.char8 = 2;
         }
         
         if (hasUpperCase(val)){
          $scope.capital = 1;
         } else {
          $scope.capital = 2;
         }
         
         if (hasNumber(val)){
          $scope.isNum = 1;
         } else {
          $scope.isNum = 2;
         }
        }
        
        function hasUpperCase(str) {
            return (/[A-Z]/.test(str));
        }
        function hasNumber(myString) {
           return /\d/.test(myString);
         }
        
        $scope.compare =function(){
         var val = $scope.changepwd.confirmnewpass;
         if($scope.changepwd.newpass == val){
          $scope.isMatch =1; 
         }else{
          $scope.isMatch = 2;
          
         }
        }
       }])

.controller('changePasswordCtrl', ['$scope', '$stateParams', '$rootScope',
function ($scope, $stateParams, $rootScope) {
	$scope.showGenericLogo = false;
    $scope.showHondoLogo= true;
	var collectionName = 'jsonStoreDivisionID';
	/*WL.JSONStore.get(collectionName).findAll().then(function (arrayResults) {
		//WL.Logger.debug(" JSON Store--->"+JSON.stringify(arrayResults));
		//alert("changePasswordCtrl"+arrayResults[0].json.divisionID)

		 if(arrayResults[0].json.divisionID == 'A'){
			 
			 $scope.showGenericLogo = false;
             $scope.showHondoLogo= true;
         }else {
         	$scope.showGenericLogo = true;
             $scope.showHondoLogo= false;
         }     
			
	}).fail(function (error) {
	});*/
		
	$scope.resetForm = function ()
    {
		$scope.changepwd.dealer =  '';
		$scope.changepwd.newpass =  '';
		$scope.changepwd.confirmnewpass =  '';
    };
 $scope.char8=0;
 $scope.capital=0;
 $scope.isNum=0;
 $scope.isMatch =0;
 $scope.changepwd={};
 $scope.validate =  function (){
   var val = $scope.changepwd.newpass;
  if (val.length>=8){
   $scope.char8 = 1;
  } else {
   $scope.char8 = 2;
  }
  
  if (hasUpperCase(val)){
   $scope.capital = 1;
  } else {
   $scope.capital = 2;
  }
  
  if (hasNumber(val)){
   $scope.isNum = 1;
  } else {
   $scope.isNum = 2;
  }
 }
 
 function hasUpperCase(str) {
     return (/[A-Z]/.test(str));
 }
 function hasNumber(myString) {
    return /\d/.test(myString);
  }
 
 $scope.compare =function(){
  var val = $scope.changepwd.confirmnewpass;
  if($scope.changepwd.newpass == val){
   $scope.isMatch =1; 
  }else{
   $scope.isMatch = 2;
   
  }
 }
}])

.controller('resetPasswordCtrl', function ($scope, $state, hondaService, $rootScope) {
	$scope.showGenericLogo = false;
    $scope.showHondoLogo= true;
	var collectionName = 'jsonStoreDivisionID';
	WL.JSONStore.get(collectionName).findAll().then(function (arrayResults) {
		//WL.Logger.debug(" JSON Store--->"+JSON.stringify(arrayResults));
		alert("resetPasswordCtrl"+arrayResults[0].json.divisionID)

		 if(arrayResults[0].json.divisionID == 'A'){
			 
			 $scope.showGenericLogo = false;
             $scope.showHondoLogo= true;
         }else {
         	$scope.showGenericLogo = true;
             $scope.showHondoLogo= false;
         }     
			
	}).fail(function (error) {
	});
		
	
		$scope.adminList = ["TRAIN8 TRAIN", "TRAIN6 TRAIN", "TRAIN27 TRAIN"];	
	    $scope.toggleSomething = function(){
	     $scope.isVisible = !$scope.isVisible;
	     console.log('make sure toggleSomething() is firing*');
	   }
	   $scope.submit = function(form) {
		   var dealerValue = $scope.dealer;
	   	   var userValue = $scope.user;
	   	if(typeof dealerValue === "undefined" || typeof userValue === "undefined" )
		{		
			$scope.error = "All authentication parameters must be provided!";
			$state.go('reset');
		}else{
			$scope.response = hondaService.resetPassword(dealerValue, userValue);
			WL.Logger.debug("ResetPwd Data --->"+angular.toJson($scope.response));
			  var res = $scope.response;
			  if(res.serviceresult.success == false){					
				  $state.go('login');					
			  }else{
				  $scope.error = "No data availble.";
				  $state.go('reset');
			  } 
		}
	   }
})

.controller('genericResetPasswordCtrl', function ($scope, $state, hondaService, $rootScope) {
	$scope.showGenericLogo = true;
    $scope.showHondoLogo= false;
	var collectionName = 'jsonStoreDivisionID';
	WL.JSONStore.get(collectionName).findAll().then(function (arrayResults) {
		//WL.Logger.debug(" JSON Store--->"+JSON.stringify(arrayResults));
		alert("genericResetPasswordCtrl"+arrayResults[0].json.divisionID)

		 if(arrayResults[0].json.divisionID == 'A'){
			 
			 $scope.showGenericLogo = false;
             $scope.showHondoLogo= true;
         }else {
         	$scope.showGenericLogo = true;
             $scope.showHondoLogo= false;
         }     
			
	}).fail(function (error) {
	});
		
	
		$scope.adminList = ["TRAIN8 TRAIN", "TRAIN6 TRAIN", "TRAIN27 TRAIN"];	
	    $scope.toggleSomething = function(){
	     $scope.isVisible = !$scope.isVisible;
	     console.log('make sure toggleSomething() is firing*');
	   }
	   $scope.submit = function(form) {
		   var dealerValue = $scope.dealer;
	   	   var userValue = $scope.user;
	   	if(typeof dealerValue === "undefined" || typeof userValue === "undefined" )
		{		
			$scope.error = "All authentication parameters must be provided!";
			$state.go('reset');
		}else{
			$scope.response = hondaService.resetPassword(dealerValue, userValue);
			WL.Logger.debug("ResetPwd Data --->"+angular.toJson($scope.response));
			  var res = $scope.response;
			  if(res.serviceresult.success == false){					
				  $state.go('login');					
			  }else{
				  $scope.error = "No data availble.";
				  $state.go('reset');
			  } 
		}
	   }
})
.controller('fAQCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {


}])

.controller('newssubmenuCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {


}])


.controller('feedbackCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {


}])


.controller('feedbackthankyouCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {


}])


.controller('storeCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {


}])

.controller('newsCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {


}])

.controller('storeCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {


}])

.controller('rest-confirmationCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {


}])



.controller('adminlistCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {
	
	
}])


