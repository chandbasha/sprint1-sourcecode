angular.module('app.services', [])
.factory('hondaService', [ DealerService ]);
function DealerService() {
	

	var factory = {};

	factory.validateDealer = validateDealer;
	factory.forgotPassword = forgotPassword;
	factory.resetPassword = resetPassword;
	factory.replaceJSONData = replaceJSONData;
	factory.replaceAcuraJSONData = replaceAcuraJSONData;
	factory.authenticate = authenticate;
	factory.homeData = homeData;
	factory.hondaSalesHomeData = hondaSalesHomeData;
	factory.hondaServiceHomeData = hondaServiceHomeData;

	return factory;
	
	function authenticate(dealerNumber, userId, password){
		var result = "NOT_MATCHED" ;
		//result  = adapter.authenticate(dealerNumber,userId, password);			
		if( dealerNumber == '206501' && userId == "ispace2"){ //HondaLogin1: 206501 / ispace2 / iNmobile1 - Internet Sales Manager -  All feature access
		   sessionId = '183b657e-2ffd-409c-8a66-0d9efcd4bdda';
		   logonID  = 'ispace2';
		   result = replaceJSONData(sessionId,logonID); 
		}else if(dealerNumber == '206501' && userId == "ispace3"){// HondaLogin2: 206501 / ispace3 / iNmobile1 - Sales Personal - Dealer Dashboard and Vehicle Locator
		   sessionId = '183b657e-2ffd-409c-8a66-0d9efcd4bddb';
		   logonID  = 'ispace3';
		   result = replaceJSONData(sessionId,logonID);  
		 }else if(dealerNumber == '206501' && userId == "ispace4"){ //HondaLogin3: 206501 / ispace4 / iNmobile1 - Service Personal - Dealer Dashboard and VIN Inquiry
		   sessionId = '183b657e-2ffd-409c-8a66-0d9efcd4bddc';
		   logonID  = 'ispace4';
		   result = replaceJSONData(sessionId,logonID);  
		 }else if(dealerNumber == '251174' && userId == "ispace2"){ //AcuraLogin1: 251174 / ispace2 / iNmobile1 - Internet Sales Manager - All feature access
			   sessionId = '183b657e-2ffd-409c-8a66-0d9efcd4bdrr';
			   dlrNo  = '251174';
			   logonID = 'ispace2';
			   actAsDlrNo = '251174';
			   dlrName = 'Acura Dealer';
			   prodDivCode = 'B';
			   currentDivision = 'B';
			   groupDivision = 'B';
			   result = replaceAcuraJSONData(sessionId, logonID, dlrNo, actAsDlrNo, dlrName, prodDivCode, currentDivision, groupDivision);  
	     }else if(dealerNumber == '251174' && userId == "ispace3"){ //AcuraLogin2: 251174 / ispace3 / iNmobile1 - Sales Personal - Dealer Dashboard and Vehicle Locator
	    	 sessionID = '183b657e-2ffd-409c-8a66-0d9efcd4bdra';
	    	 dlrNo = '251174';
	    	 logonID = 'ispace3';
	    	 actAsDlrNo = '251174';
	    	 dlrName = 'Acura Dealer';
	    	 prodDivCode = 'B';
	    	 currentDivision = 'B';
	    	 groupDivision = 'B';
	    	 result = replaceAcuraJSONData(sessionId, logonID, dlrNo, actAsDlrNo, dlrName, prodDivCode, currentDivision, groupDivision);   
	     }else if(dealerNumber == '251174' && userId == "ispace4"){ //AcuraLogin3: 251174 / ispace4 / iNmobile1 - Service Personal - Dealer Dashboard and VIN Inquiry
	    	 sessionID = '183b657e-2ffd-409c-8a66-0d9efcd4bdrb';
	    	 dlrNo = '251174';
	    	 logonID = 'ispace4';
	    	 actAsDlrNo = '251174';
	    	 dlrName = 'Acura Dealer';
	    	 prodDivCode = 'B';
	    	 currentDivision = 'B';
	    	 groupDivision = 'B';
	    	 result = replaceAcuraJSONData(sessionId, logonID, dlrNo, actAsDlrNo, dlrName, prodDivCode, currentDivision, groupDivision); 
	     }
		
		return result;
	}
	
	
	function replaceJSONData(sessionId,loginId){
		  var replaceBy = {
		    SessionID: function() {		      
		          return sessionId		        
		      },
		      LogonID: function() {		       
		          return loginId
		        }
		    }
		  var res = JSON.stringify(actualJSONData, function(key, value) {
		      if(replaceBy[key]) {
		        value = replaceBy[key](value)
		      }
		      return value
		    })
		return res;	
	}
	
	function replaceAcuraJSONData(sessionId, loginId, dlrNo, actAsDlrNo, dlrName, prodDivCode, currentDivision, groupDivision){
		  var replaceBy = {
		      SessionID: function() {		      
		          return sessionId		        
		      },
		      LogonID: function() {		       
		          return loginId
		      },
		      DlrNo: function() {		       
		          return dlrNo
		      },
		      ActAsDlrNo: function() {		       
		          return actAsDlrNo
		      },
		      DlrName: function() {		       
		          return dlrName
		      },
		      ProdDivCode: function() {		       
		          return prodDivCode
		      },
		      CurrentDivision: function() {		       
		          return currentDivision
		      },
		      GroupDivision: function() {		       
		          return groupDivision
		      }
		    }
		  var res = JSON.stringify(actualJSONData, function(key, value) {
		      if(replaceBy[key]) {
		        value = replaceBy[key](value)
		      }
		      return value
		    })
		return res;	
	}

	function validateDealer() {
		return validateDealerUserIdResult;
	}

	function forgotPassword(dealerNumber, userId) {
		return forgotPasswordResult;
	}
	
	function resetPassword() {		
		return resetPasswordResult;
	}
	
	function homeData(){
		
	    return homeJson;
	}
	
	function hondaSalesHomeData(){
		return hondaSalesHomeJson;
	}
	
	function hondaServiceHomeData(){
		return hondaServiceHomeJson
	}
};


