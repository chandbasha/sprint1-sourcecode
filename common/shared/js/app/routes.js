angular.module('app.routes', ['ionicUIRouter'])

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider
    
  

      /* 
    The IonicUIRouter.js UI-Router Modification is being used for this route.
    To navigate to this route, do NOT use a URL. Instead use one of the following:
      1) Using the ui-sref HTML attribute:
        ui-sref='tabsController.hONDA'
      2) Using $state.go programatically:
        $state.go('tabsController.hONDA');
    This allows your app to figure out which Tab to open this page in on the fly.*/
  .state('tabsController.hONDA', {
    url: '/home',
    views: {
      'tab4': {
        templateUrl: 'templates/hONDA.html',
        controller: 'hONDACtrl'
      },
      'tab2': {
        templateUrl: 'templates/hONDA.html',
        controller: 'hONDACtrl'
      },
      'tab3': {
        templateUrl: 'templates/hONDA.html',
        controller: 'hONDACtrl'
      }
    }
  })

  .state('tabsController', {
    url: '/page1',
    templateUrl: 'templates/tabsController.html',
    abstract:true
  })

  /*.state('tabs', {
	  templateUrl: 'templates/hONDA.html',
      controller: 'hONDACtrl'
  })*/
  /* 
    The IonicUIRouter.js UI-Router Modification is being used for this route.
    To navigate to this route, do NOT use a URL. Instead use one of the following:
      1) Using the ui-sref HTML attribute:
        ui-sref='tabsController.dealerSubmenu'
      2) Using $state.go programatically:
        $state.go('tabsController.dealerSubmenu');
    This allows your app to figure out which Tab to open this page in on the fly.*/
  .state('tabsController.dealerSubmenu', {
    url: '/Dealer-Submenu',
    views: {
      'tab4': {
        templateUrl: 'templates/dealerSubmenu.html',
        controller: 'dealerSubmenuCtrl'
      },
      'tab2': {
        templateUrl: 'templates/dealerSubmenu.html',
        controller: 'dealerSubmenuCtrl'
      },
      'tab3': {
        templateUrl: 'templates/dealerSubmenu.html',
        controller: 'dealerSubmenuCtrl'
      }
    }
  })

  /* 
    The IonicUIRouter.js UI-Router Modification is being used for this route.
    To navigate to this route, do NOT use a URL. Instead use one of the following:
      1) Using the ui-sref HTML attribute:
        ui-sref='tabsController.dealerDashboard'
      2) Using $state.go programatically:
        $state.go('tabsController.dealerDashboard');
    This allows your app to figure out which Tab to open this page in on the fly.*/
  .state('tabsController.dealerDashboard', {
    url: '/dealer dashboard',
    views: {
      'tab4': {
        templateUrl: 'templates/dealerDashboard.html',
        controller: 'dealerDashboardCtrl'
      },
      'tab2': {
        templateUrl: 'templates/dealerDashboard.html',
        controller: 'dealerDashboardCtrl'
      },
      'tab3': {
        templateUrl: 'templates/dealerDashboard.html',
        controller: 'dealerDashboardCtrl'
      }
    }
  })
  
    .state('login', {
    url: '/login',
    templateUrl: 'shared/views/login.html',
  //  controller: 'login-genericCtrl'
 })


  .state('login-generic', {
  url: '/login-generic',
  templateUrl: 'templates/login-generic.html',
  //controller: 'login-genericCtrl'
  })
  
 /*  .state('login-home', {
  url: '/login-home',
  templateUrl: 'templates/hONDA.html',
  controller: 'login-home'
  })*/

 .state('changePassword', {
    url: '/change',
    templateUrl: 'templates/changePassword.html',
    controller: 'changePasswordCtrl'
  })

  

  .state('genericChangePassword', {
	    url: '/change',
	    templateUrl: 'templates/changePassword.html',
	    controller: 'genericChangePasswordCtrl'
	  })
	  
  .state('resetPassword', {
    url: '/reset',
    templateUrl: 'templates/resetPassword.html',
    controller: 'resetPasswordCtrl'
  })
  
  .state('genericResetPassword', {
    url: '/reset',
    templateUrl: 'templates/resetPassword.html',
    controller: 'genericResetPasswordCtrl'
  })


  
    /* 
    The IonicUIRouter.js UI-Router Modification is being used for this route.
    To navigate to this route, do NOT use a URL. Instead use one of the following:
      1) Using the ui-sref HTML attribute:
        ui-sref='tabsController.fAQ'
      2) Using $state.go programatically:
        $state.go('tabsController.fAQ');
    This allows your app to figure out which Tab to open this page in on the fly.*/
  .state('tabsController.fAQ', {
    url: '/fAQ',
    views: {
      'tab4': {
        templateUrl: 'templates/fAQ.html',
        controller: 'fAQCtrl'
      },
      'tab2': {
        templateUrl: 'templates/fAQ.html',
        controller: 'fAQCtrl'
      },
      'tab3': {
        templateUrl: 'templates/fAQ.html',
        controller: 'fAQCtrl'
      }
    }
  })
  
  
  
    /* 
    The IonicUIRouter.js UI-Router Modification is being used for this route.
    To navigate to this route, do NOT use a URL. Instead use one of the following:
      1) Using the ui-sref HTML attribute:
        ui-sref='tabsController.vIN'
      2) Using $state.go programatically:
        $state.go('tabsController.vIN');
    This allows your app to figure out which Tab to open this page in on the fly.*/
  .state('tabsController.vIN', {
    url: '/vIN',
    views: {
      'tab2': {
        templateUrl: 'templates/vIN.html',
        controller: 'vINCtrl'
      },
      'tab3': {
        templateUrl: 'templates/vIN.html',
        controller: 'vINCtrl'
      }
    }
  })
  
    /* 
    The IonicUIRouter.js UI-Router Modification is being used for this route.
    To navigate to this route, do NOT use a URL. Instead use one of the following:
      1) Using the ui-sref HTML attribute:
        ui-sref='tabsController.feedback'
      2) Using $state.go programatically:
        $state.go('tabsController.feedback');
    This allows your app to figure out which Tab to open this page in on the fly.*/
  .state('tabsController.feedback', {
    url: '/feedback',
    views: {
      'tab2': {
        templateUrl: 'templates/feedback.html',
        controller: 'feedbackCtrl'
      },
      'tab3': {
        templateUrl: 'templates/feedback.html',
        controller: 'feedbackCtrl'
      }
    }
  })
  
      /* 
    The IonicUIRouter.js UI-Router Modification is being used for this route.
    To navigate to this route, do NOT use a URL. Instead use one of the following:
      1) Using the ui-sref HTML attribute:
        ui-sref='tabsController.feedback'
      2) Using $state.go programatically:
        $state.go('tabsController.feedback');
    This allows your app to figure out which Tab to open this page in on the fly.*/
  .state('tabsController.store', {
    url: '/store',
    views: {
      'tab2': {
        templateUrl: 'templates/store.html',
        controller: 'storeCtrl'
      },
      'tab3': {
        templateUrl: 'templates/store.html',
        controller: 'storeCtrl'
      }
    }
  })
  
  
        /* 
    The IonicUIRouter.js UI-Router Modification is being used for this route.
    To navigate to this route, do NOT use a URL. Instead use one of the following:
      1) Using the ui-sref HTML attribute:
        ui-sref='tabsController.feedback'
      2) Using $state.go programatically:
        $state.go('tabsController.feedback');
    This allows your app to figure out which Tab to open this page in on the fly.*/
  .state('tabsController.news', {
    url: '/news',
    views: {
      'tab2': {
        templateUrl: 'templates/news.html',
        controller: 'newsCtrl'
      },
      'tab3': {
        templateUrl: 'templates/news.html',
        controller: 'newsCtrl'
      }
    }
  })
  
  
        /* 
    The IonicUIRouter.js UI-Router Modification is being used for this route.
    To navigate to this route, do NOT use a URL. Instead use one of the following:
      1) Using the ui-sref HTML attribute:
        ui-sref='tabsController.feedback'
      2) Using $state.go programatically:
        $state.go('tabsController.feedback');
    This allows your app to figure out which Tab to open this page in on the fly.*/
  .state('tabsController.newssubmenu', {
    url: '/newssubmenu',
    views: {
      'tab2': {
        templateUrl: 'templates/newssubmenu.html',
        controller: 'newssubmenuCtrl'
      },
      'tab3': {
        templateUrl: 'templates/newssubmenu.html',
        controller: 'newssubmenuCtrl'
      }
    }
  })
  
    /* 
    The IonicUIRouter.js UI-Router Modification is being used for this route.
    To navigate to this route, do NOT use a URL. Instead use one of the following:
      1) Using the ui-sref HTML attribute:
        ui-sref='tabsController.feedback'
      2) Using $state.go programatically:
        $state.go('tabsController.feedback');
    This allows your app to figure out which Tab to open this page in on the fly.*/
  .state('tabsController.feedbackthankyou', {
    url: '/feedbackthankyou',
    views: {
      'tab2': {
        templateUrl: 'templates/feedbackthankyou.html',
        controller: 'feedbackthankyouCtrl'
      },
      'tab3': {
        templateUrl: 'templates/feedbackthankyou.html',
        controller: 'feedbackthankyouCtrl'
      }
    }
  })
  
      /* 
    The IonicUIRouter.js UI-Router Modification is being used for this route.
    To navigate to this route, do NOT use a URL. Instead use one of the following:
      1) Using the ui-sref HTML attribute:
        ui-sref='tabsController.feedback'
      2) Using $state.go programatically:
        $state.go('tabsController.feedback');
    This allows your app to figure out which Tab to open this page in on the fly.*/
  .state('tabsController.changepassword-confirmation', {
    url: '/changepassword-confirmation',
    views: {
      'tab2': {
        templateUrl: 'templates/changepassword-confirmation.html',
        controller: 'changepassword-confirmationCtrl'
      },
      'tab3': {
        templateUrl: 'templates/changepassword-confirmation.html',
        controller: 'changepassword-confirmationCtrl'
      }
    }
  })
  
      /* 
    The IonicUIRouter.js UI-Router Modification is being used for this route.
    To navigate to this route, do NOT use a URL. Instead use one of the following:
      1) Using the ui-sref HTML attribute:
        ui-sref='tabsController.feedback'
      2) Using $state.go programatically:
        $state.go('tabsController.feedback');
    This allows your app to figure out which Tab to open this page in on the fly.*/
  .state('tabsController.rest-confirmation', {
    url: '/rest-confirmation',
    views: {
      'tab2': {
        templateUrl: 'templates/rest-confirmation.html',
        controller: 'rest-confirmationCtrl'
      },
      'tab3': {
        templateUrl: 'templates/rest-confirmation.html',
        controller: 'rest-confirmationCtrl'
      }
    }
  })
  
        /* 
    The IonicUIRouter.js UI-Router Modification is being used for this route.
    To navigate to this route, do NOT use a URL. Instead use one of the following:
      1) Using the ui-sref HTML attribute:
        ui-sref='tabsController.feedback'
      2) Using $state.go programatically:
        $state.go('tabsController.feedback');
    This allows your app to figure out which Tab to open this page in on the fly.*/
  .state('tabsController.adminlist', {
    url: '/adminlist',
    views: {
      'tab2': {
        templateUrl: 'templates/adminlist.html',
        controller: 'adminlistCtrl'
      },
      'tab3': {
        templateUrl: 'templates/adminlist.html',
        controller: 'adminlistCtrl'
      }
    }
  })
  
  
  

//$urlRouterProvider.otherwise('/login')
$urlRouterProvider.otherwise('/login-generic')
  

});