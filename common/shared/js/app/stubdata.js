var validateDealerUserIdResult = {
		"serviceresult" : {
			"success" : false,
			"message" : "",
			"data" : {
				"division" : "A"
			}
		}
	};

	var forgotPasswordResult = {
		"serviceresult" : {
			"success" : false,
			"message" : "",
			"data" : {
				"success" : false
			}
		}
	};

	var resetPasswordResult = {
		"serviceresult" : {
			"success" : false,
			"message" : "",
			"data" : {
				"success" : false
			}
		}
	};
	
	var actualJSONData = {
			   "serviceresult": {
			    "success": true,
			    "message": "",
			    "data": {
			     "sessioninfo": {
			      "SessionID": "183b657e-2ffd-409c-8a66-0d9efcd4bdda",
			      "SessionStartTime": "2016-11-22T15:30:00",
			      "SessionEndTime": "2016-11-22T16:30:00",
			      "SessionLastUpdateTime": "2016-11-22T15:30:00",
			      "DlrNo": "206501",
			      "LogonID": "ispace2",
			      "FirstName": "",
			      "LastName": "",
			      "ActAsDlrNo": "206501",
			      "DlrName": "MANLY HONDA",
			      "DlrState": "CA",
			      "ProdDivCode": "A",
			      "CurrentDivision": "A ",
			      "GroupDivision": "A",
			      "SlsZoneCode": "12 ",
			      "IntlRegionCode": "U ",
			      "Language": 0,
			      "PartType":"P ",
			      "Active": true,
			      "Admin": true,
			      "AdminType": 2,
			      "Reset": false,
			      "ResetPassword": 0,
			      "ISISRoleID": 3,
			      "IPAddressWS": "10.235 .114 .31 ",
			      "DlrType": "RD ",
			      "PEPrimaryProduct": "",
			      "SalesProdLines": "0000000000000000000000000000000000000000 ",
			      "ServicePartsProdLines": "0000000000000000000000000000000000000000 ",
			      "ServiceZone": "12 ",
			      "PartsZone": "12 ",
			      "PartsDist": "A ",
			      "MenuDivision": "AA ",
			      "OUMenuDivision": "OA ",
			      "ISISMenuDivision": "SA ",
			      "SLS_DIST_CD": "A ",
			      "DPTSID": "",
			      "SessionVariables": {
			       "salesagreement": "CH1,",
			       "oprimaryhost": "unittest.in.honda.com ",
			       "secondaryhost": "indxwuniiis01.amerhonda.com ",
			       "stylesheet": "repae11a.css ",
			       "computername": "UNKNOWN ",
			       "partnoactivationdate": "2 / 22 / 2010 ",
			       "stylesheetpe": "repae11a.css ",
			       "firsttimesignin": "0 ",
			       "osecondaryhost": "unittest.in.honda.com ",
			       "mobiledevice": "N ",
			       "primaryhost": "indxwuniiis01.amerhonda.com "
			      },
			      "SessionInformationVariables ": {

			       "sessionid ": "183 b657e - 2 ffd - 409 c - 8 a66 - 0 d9efcd4bdda ",
			       "firstname ": "",
			       "logonid ": "AHM ",
			       "reset ": "0 ",
			       "dptsid ": "",
			       "sessionstarttime ": "11 / 22 / 2016 3: 30: 00 PM ",
			       "sessionendtime ": "11 / 22 / 2016 4: 30: 00 PM ",
			       "dlrstate ": "CA ",
			       "partszone ": "12 ",
			       "sessionvar ": "System.Collections.Hashtable ",
			       "dlrno ": "206501 ",
			       "menudivision ": "AA ",
			       "groupdivision ": "A ",
			       "actasdlrno ": "206501 ",
			       "dlrname ": "MANLY HONDA ",
			       "partsdist ": "A ",
			       "servicezone ": "12 ",
			       "lastname ": "",
			       "salesprodlines ": "0000000000000000000000000000000000000000 ",
			       "oumenudivision ": "OA ",
			       "servicepartsprodlines ": "0000000000000000000000000000000000000000 ",
			       "sls_dist_cd ": "A ",
			       "isismenudivision ": "SA ",
			       "slszonecode ": "12 ",
			       "ipaddressws ": "10.235 .114 .31 ",
			       "dlrtype ": "RD ",
			       "peprimaryproduct ": "",
			       "admin ": "2 ",
			       "sessionlastupdatetime ": "11 / 22 / 2016 3: 30: 00 PM ",
			       "currentdivision ": "A ",
			       "isisroleid ": "3 ",
			       "parttype ": "P ",
			       "language ": "0 ",
			       "servicedist ": "A ",
			       "active ": "1 ",
			       "intlregioncode ": "U ",
			       "proddivcode ": "A "
			      },
			      "SessionInformationVariablesWS ": [{}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}],
			      "ThemesPath ": " / RRXAAPPS / Shared / Themes / Honda "
			     }
			    }
			   }
			  }

	var homeJson = {
			"serviceresult": {
				       
					 	"success": false,
					 	"message": "",
					 	"data": {
					 		"AppFolder": {
					 			"FolderId": 0,
					 			"DisplayOrder": "asc",
					 			"SlideBox": {
					 				"Slide": [{
					 					"SlideId": 0,
					 					"DisplayOrder": "asc",
					 					"AppIconList": [{
					 						"Title": "Dealer DashBoard",
					 						"ImageUrl": "Dealer-Dashboard-Icon.png",
					 						"IsFolder": false,
					 						"FolderId": 0,
					 						"DisplayOrder": "asc"
					 					}, {
					 						"Title": "Latest News",
					 						"ImageUrl": "Latest-News-Icon.png",
					 						"IsFolder": false,
					 						"FolderId": 0,
					 						"DisplayOrder": "asc"
					 					}, {
					 						"Title": "Vehicle Locator",
					 						"ImageUrl": "Locator-Icon.png",
					 						"IsFolder": false,
					 						"FolderId": 0,
					 						"DisplayOrder": "asc"
					 					}, {
					 						"Title": "VIN Inquiry",
					 						"ImageUrl": "VIN-Inquiry-Icon.png",
					 						"IsFolder": false,
					 						"FolderId": 0,
					 						"DisplayOrder": "asc"
					 					}, {
					 						"Title": "Facts Guides",
					 						"ImageUrl": "Facts-Guides-Icon.png",
					 						"IsFolder": false,
					 						"FolderId": 0,
					 						"DisplayOrder": "asc"
					 					}, {
					 						"Title": "RTM Mobile",
					 						"ImageUrl": "RTM-Mobile-Icon.png",
					 						"IsFolder": false,
					 						"FolderId": 0,
					 						"DisplayOrder": "asc"
					 					}, {
					 						"Title": "FOC Mobile",
					 						"ImageUrl": "FOC-Mobile-Icon.png",
					 						"IsFolder": false,
					 						"FolderId": 0,
					 						"DisplayOrder": "asc"
					 					}, {
					 						"Title": "HSE Mobile",
					 						"ImageUrl": "HSE-Mobile-Icon.png",
					 						"IsFolder": false,
					 						"FolderId": 0,
					 						"DisplayOrder": "asc"
					 					}, {
					 						"Title": "Tech Line",
					 						"ImageUrl": "Tech-Line-Icon.png",
					 						"IsFolder": false,
					 						"FolderId": 0,
					 						"DisplayOrder": "asc"
					 					}, {
					 						"Title": "Store",
					 						"ImageUrl": "Store-Icon.png",
					 						"IsFolder": false,
					 						"FolderId": 0,
					 						"DisplayOrder": "asc"
					 					}, {
					 						"Title": "Feedback",
					 						"ImageUrl": "feedback-Icon.png",
					 						"IsFolder": false,
					 						"FolderId": 0,
					 						"DisplayOrder": "asc"
					 					}, {
					 						"Title": "FAQ",
					 						"ImageUrl": "FAQ-Icon.png",
					 						"IsFolder": false,
					 						"FolderId": 0,
					 						"DisplayOrder": "asc"
					 					}]
					 				}, {
					 					"SlideId": 1,
					 					"DisplayOrder": "asc",
					 					"AppIconList": [{
					 						"Title": "CSAP",
					 						"ImageUrl": "CPSP-Icon.png",
					 						"IsFolder": false,
					 						"FolderId": 0,
					 						"DisplayOrder": "asc"
					 					}, {
					 						"Title": "Learning Center",
					 						"ImageUrl": "Learning-Center-Icon.png",
					 						"IsFolder": false,
					 						"FolderId": 0,
					 						"DisplayOrder": "asc"
					 					}]
					 				}]

					 			}
					 		}
					 	}
					 }
			   
			};


	var hondaSalesHomeJson = {
		"serviceresult":{ 
		 	"success": false,
		 	"message": "",
		 	"data": {
		 		"AppFolder": {
		 			"FolderId": 0,
		 			"DisplayOrder": "asc",
		 			"SlideBox": {
		 				"Slide": [{
		 					"SlideId": 0,
		 					"DisplayOrder": "asc",
		 					"AppIconList": [{
		 						"Title": "Dealer DashBoard",
		 						"ImageUrl": "Dealer-Dashboard-Icon.png",
		 						"IsFolder": false,
		 						"FolderId": 0,
		 						"DisplayOrder": "asc"
		 					}, {
		 						"Title": "Latest News",
		 						"ImageUrl": "Latest-News-Icon.png",
		 						"IsFolder": false,
		 						"FolderId": 0,
		 						"DisplayOrder": "asc"
		 					}, {
		 						"Title": "VIN Inquiry",
		 						"ImageUrl": "VIN-Inquiry-Icon.png",
		 						"IsFolder": false,
		 						"FolderId": 0,
		 						"DisplayOrder": "asc"
		 					}, {
		 						"Title": "Tech Line",
		 						"ImageUrl": "Tech-Line-Icon.png",
		 						"IsFolder": false,
		 						"FolderId": 0,
		 						"DisplayOrder": "asc"
		 					}, {
		 						"Title": "Contact Us",
		 						"ImageUrl": "Contact-Icon.png",
		 						"IsFolder": false,
		 						"FolderId": 0,
		 						"DisplayOrder": "asc"
		 					}, {
		 						"Title": "FAQ",
		 						"ImageUrl": "FAQ-Icon.png",
		 						"IsFolder": false,
		 						"FolderId": 0,
		 						"DisplayOrder": "asc"
		 					}]
		 				}]
		 			}
		 		}
		 	}
		 }
	
	};
	var hondaServiceHomeJson ={
		"serviceresult":{ 
		 	"success": false,
		 	"message": "",
		 	"data": {
		 		"AppFolder": {
		 			"FolderId": 0,
		 			"DisplayOrder": "asc",
		 			"SlideBox": {
		 				"Slide": [{
		 					"SlideId": 0,
		 					"DisplayOrder": "asc",
		 					"AppIconList": [{
		 						"Title": "Latest News",
		 						"ImageUrl": "Latest-News-Icon.png",
		 						"IsFolder": false,
		 						"FolderId": 0,
		 						"DisplayOrder": "asc"
		 					}, {
		 						"Title": "Vehicle Locator",
		 						"ImageUrl": "Locator-Icon.png",
		 						"IsFolder": false,
		 						"FolderId": 0,
		 						"DisplayOrder": "asc"
		 					}, {
		 						"Title": "Facts Guides",
		 						"ImageUrl": "Facts-Guides-Icon.png",
		 						"IsFolder": false,
		 						"FolderId": 0,
		 						"DisplayOrder": "asc"
		 					}, {
		 						"Title": "HSE Mobile",
		 						"ImageUrl": "HSE-Mobile-Icon.png",
		 						"IsFolder": false,
		 						"FolderId": 0,
		 						"DisplayOrder": "asc"
		 					}, {
		 						"Title": "Honda Learning Center",
		 						"ImageUrl": "Learning-Center-Icon.png",
		 						"IsFolder": false,
		 						"FolderId": 0,
		 						"DisplayOrder": "asc"
		 					}, {
		 						"Title": "Contact Us",
		 						"ImageUrl": "Contact-Icon.png",
		 						"IsFolder": false,
		 						"FolderId": 0,
		 						"DisplayOrder": "asc"
		 					}, {
		 						"Title": "FAQs",
		 						"ImageUrl": "FAQ-Icon.png",
		 						"IsFolder": false,
		 						"FolderId": 0,
		 						"DisplayOrder": "asc"
		 					}]
		 				}]
		 			}
		 		}
		 	}
		 }
	 
	};
